(function () {
    'use strict';

    angular
        .module('listingInformation')
        .directive('liAccordion', liAccordion);

    liAccordion.$inject = [];

    /* @ngInject */
    function liAccordion() {
        return {
            transclude: true,
            require: '^headerTitle',
            scope: {
                headerTitle: '@',
                isOpen: '@',
            },
            template: [
                '<div class="accordion">',
                    '<div class="accordion-header">',
                        '<div class="accordion-header-content">',
                            '<span class="accordion-switcher" data-ng-class="{\'switcher-open\': show(), \'switcher-close\': !show()}" data-ng-click="switch()"></span>',
                            '{{ headerTitle }}',
                        '</div>',
                    '</div>',
                    '<div class="accordion-content" data-ng-show="show()">',
                        '<ng-transclude></ng-transclude>',
                    '</div>',
                '</div>',
            ].join(''),
            controller: function ($scope, $element, $attrs) {
                $scope.isOpen = ($scope.isOpen === 'true');

                $scope.show = function () {
                    return $scope.isOpen == true || $scope.isOpen == 'true';
                };

                $scope.switch = function () {
                    if(typeof ($scope.isOpen) !== 'boolean') {
                        $scope.isOpen = !($scope.isOpen === 'true');
                    } else {
                        $scope.isOpen = !$scope.isOpen;
                    }
                }
            }
        };
    }
})();
